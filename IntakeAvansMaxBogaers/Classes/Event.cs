﻿using IntakeAvansMaxBogaers.Classes;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntakeAvansMaxBogaers
{
    class Event : Data
    {
        public Event(int artist, int stage, DateTime start, DateTime end)
        {
            try
            {
                con.Open();

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandText = String.Format("INSERT INTO {0} (`artist_id`, `stage_id`,`start_time`,`end_time`) VALUES (@Artist, @Stage, @Start, @End);", base.events);
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@Artist", artist);
                cmd.Parameters.AddWithValue("@Stage", stage);
                cmd.Parameters.AddWithValue("@Start", start);
                cmd.Parameters.AddWithValue("@End", end);
                cmd.ExecuteNonQuery();
                System.Windows.Forms.MessageBox.Show(String.Format("An {0} has been created", this.events.Substring(0, this.events.Length - 1)));
            }
            catch (MySqlException ex)
            {
                Debug.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }

            }
        }
       
    }
}
