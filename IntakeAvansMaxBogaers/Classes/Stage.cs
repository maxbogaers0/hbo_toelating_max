﻿using IntakeAvansMaxBogaers.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntakeAvansMaxBogaers
{
    class Stage : Data
    {
        internal Stage(String name, String description)
        {
            Create(name, description, base.stages);
        }
    }
}
