﻿using IntakeAvansMaxBogaers.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntakeAvansMaxBogaers
{
    class Artist : Data
    {
        internal Artist(String name, String description)
        {
            base.Create(name, description, base.artists);
        }
    }
}
