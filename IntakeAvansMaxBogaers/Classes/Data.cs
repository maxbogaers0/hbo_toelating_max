﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace IntakeAvansMaxBogaers.Classes
{
    internal class Data
    {
        private String conString = IntakeAvansMaxBogaers.Properties.Settings.Default.ConnectionString;
        internal MySqlConnection con;
        internal String stages = "stages";
        internal String artists = "artists";
        internal String events = "events";

        public Data()
        {
            con = new MySqlConnection(conString);
        }
        internal virtual DataTable getAll(String table)
        {
            try { 
            MySqlDataAdapter myda = new MySqlDataAdapter("SELECT * FROM "+table, con);
            DataTable returnData = new DataTable(table);
            con.Open();
            myda.Fill(returnData);
            con.Close();
            return returnData;
            }
            catch (MySqlException ex)
            {
                Debug.WriteLine("Error: {0}", ex.ToString());
                return null;
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }

            }
        }


         protected virtual void Create(String name, String description, String table)
        {
            try
            {
                con.Open();

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "INSERT INTO "+table+" (`name`, `description`) VALUES (@Name, @Description);";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Description", description);
                cmd.ExecuteNonQuery();

                System.Windows.Forms.MessageBox.Show(String.Format("An {0} has been created", table.Substring(0, table.Length - 1)));

            }
            catch (MySqlException ex)
            {
                Debug.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }

            }
        }

        internal void UpdateTable(DataTable table)
        {
            try
            {
                con.Open();
                using (MySqlTransaction tran = con.BeginTransaction(IsolationLevel.Serializable))
                {
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.Transaction = tran;
                        cmd.CommandText = "SELECT * FROM " + table.TableName;
                        using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                        {
                            da.UpdateBatchSize = 1000;
                            using (MySqlCommandBuilder cb = new MySqlCommandBuilder(da))
                            {
                                da.Update(table);
                                tran.Commit();
                            }
                        }
                    }
                }
            }
            catch (MySqlException ex)
            {
                Debug.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }

            }
        }
    }
}
