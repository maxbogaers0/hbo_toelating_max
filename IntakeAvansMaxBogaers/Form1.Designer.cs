﻿

namespace IntakeAvansMaxBogaers
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTabSelector = new MaterialSkin.Controls.MaterialTabSelector();
            this.mainTabControll = new MaterialSkin.Controls.MaterialTabControl();
            this.EventTab = new System.Windows.Forms.TabPage();
            this.materialTabSelector1 = new MaterialSkin.Controls.MaterialTabSelector();
            this.materialTabControlEvents = new MaterialSkin.Controls.MaterialTabControl();
            this.Overview = new System.Windows.Forms.TabPage();
            this.SubmitButtonEventDB = new MaterialSkin.Controls.MaterialRaisedButton();
            this.dataGridViewEvents = new System.Windows.Forms.DataGridView();
            this.Create = new System.Windows.Forms.TabPage();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.EventSubmitButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.comboBoxEventStage = new System.Windows.Forms.ComboBox();
            this.comboBoxEventArtist = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.materialTabSelector2 = new MaterialSkin.Controls.MaterialTabSelector();
            this.materialTabControlArtists = new MaterialSkin.Controls.MaterialTabControl();
            this.overviewArtists = new System.Windows.Forms.TabPage();
            this.SubmitArtistOverview = new MaterialSkin.Controls.MaterialRaisedButton();
            this.dataGridViewArtists = new System.Windows.Forms.DataGridView();
            this.ArtistCreate = new System.Windows.Forms.TabPage();
            this.SubmitButtonArtist = new MaterialSkin.Controls.MaterialRaisedButton();
            this.TextBoxArtistDesription = new System.Windows.Forms.RichTextBox();
            this.artistNameField = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.materialTabSelector3 = new MaterialSkin.Controls.MaterialTabSelector();
            this.materialTabControl2 = new MaterialSkin.Controls.MaterialTabControl();
            this.StagesOverview = new System.Windows.Forms.TabPage();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.dataGridViewStage = new System.Windows.Forms.DataGridView();
            this.CreateTabStage = new System.Windows.Forms.TabPage();
            this.submitCreationStage = new MaterialSkin.Controls.MaterialRaisedButton();
            this.DescriptionStage = new System.Windows.Forms.RichTextBox();
            this.materialSingleLineTextFieldStage = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.mainTabControll.SuspendLayout();
            this.EventTab.SuspendLayout();
            this.materialTabControlEvents.SuspendLayout();
            this.Overview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvents)).BeginInit();
            this.Create.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.materialTabControlArtists.SuspendLayout();
            this.overviewArtists.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArtists)).BeginInit();
            this.ArtistCreate.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.materialTabControl2.SuspendLayout();
            this.StagesOverview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStage)).BeginInit();
            this.CreateTabStage.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTabSelector
            // 
            this.mainTabSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainTabSelector.BaseTabControl = this.mainTabControll;
            this.mainTabSelector.Depth = 0;
            this.mainTabSelector.Location = new System.Drawing.Point(0, 33);
            this.mainTabSelector.MouseState = MaterialSkin.MouseState.HOVER;
            this.mainTabSelector.Name = "mainTabSelector";
            this.mainTabSelector.Size = new System.Drawing.Size(815, 64);
            this.mainTabSelector.TabIndex = 0;
            this.mainTabSelector.Text = "materialTabSelector1";
            // 
            // mainTabControll
            // 
            this.mainTabControll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainTabControll.Controls.Add(this.EventTab);
            this.mainTabControll.Controls.Add(this.tabPage2);
            this.mainTabControll.Controls.Add(this.tabPage3);
            this.mainTabControll.Depth = 0;
            this.mainTabControll.Location = new System.Drawing.Point(0, 94);
            this.mainTabControll.MouseState = MaterialSkin.MouseState.HOVER;
            this.mainTabControll.Name = "mainTabControll";
            this.mainTabControll.SelectedIndex = 0;
            this.mainTabControll.Size = new System.Drawing.Size(815, 361);
            this.mainTabControll.TabIndex = 1;
            // 
            // EventTab
            // 
            this.EventTab.Controls.Add(this.materialTabSelector1);
            this.EventTab.Controls.Add(this.materialTabControlEvents);
            this.EventTab.Location = new System.Drawing.Point(4, 22);
            this.EventTab.Name = "EventTab";
            this.EventTab.Padding = new System.Windows.Forms.Padding(3);
            this.EventTab.Size = new System.Drawing.Size(807, 335);
            this.EventTab.TabIndex = 0;
            this.EventTab.Text = "Events";
            this.EventTab.UseVisualStyleBackColor = true;
            // 
            // materialTabSelector1
            // 
            this.materialTabSelector1.BaseTabControl = this.materialTabControlEvents;
            this.materialTabSelector1.Depth = 0;
            this.materialTabSelector1.Location = new System.Drawing.Point(0, 0);
            this.materialTabSelector1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabSelector1.Name = "materialTabSelector1";
            this.materialTabSelector1.Size = new System.Drawing.Size(807, 91);
            this.materialTabSelector1.TabIndex = 1;
            this.materialTabSelector1.Text = "materialTabSelector1";
            // 
            // materialTabControlEvents
            // 
            this.materialTabControlEvents.Controls.Add(this.Overview);
            this.materialTabControlEvents.Controls.Add(this.Create);
            this.materialTabControlEvents.Depth = 0;
            this.materialTabControlEvents.Location = new System.Drawing.Point(0, 91);
            this.materialTabControlEvents.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControlEvents.Name = "materialTabControlEvents";
            this.materialTabControlEvents.SelectedIndex = 0;
            this.materialTabControlEvents.Size = new System.Drawing.Size(805, 270);
            this.materialTabControlEvents.TabIndex = 0;
            // 
            // Overview
            // 
            this.Overview.Controls.Add(this.SubmitButtonEventDB);
            this.Overview.Controls.Add(this.dataGridViewEvents);
            this.Overview.Location = new System.Drawing.Point(4, 22);
            this.Overview.Name = "Overview";
            this.Overview.Padding = new System.Windows.Forms.Padding(3);
            this.Overview.Size = new System.Drawing.Size(797, 244);
            this.Overview.TabIndex = 0;
            this.Overview.Text = "Overview";
            this.Overview.UseVisualStyleBackColor = true;
            this.Overview.Enter += new System.EventHandler(this.Overview_Enter);
            this.Overview.Leave += new System.EventHandler(this.Overview_Leave);
            // 
            // SubmitButtonEventDB
            // 
            this.SubmitButtonEventDB.AutoSize = true;
            this.SubmitButtonEventDB.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SubmitButtonEventDB.Depth = 0;
            this.SubmitButtonEventDB.Icon = null;
            this.SubmitButtonEventDB.Location = new System.Drawing.Point(0, 191);
            this.SubmitButtonEventDB.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubmitButtonEventDB.Name = "SubmitButtonEventDB";
            this.SubmitButtonEventDB.Primary = true;
            this.SubmitButtonEventDB.Size = new System.Drawing.Size(71, 36);
            this.SubmitButtonEventDB.TabIndex = 1;
            this.SubmitButtonEventDB.Text = "Submit";
            this.SubmitButtonEventDB.UseVisualStyleBackColor = true;
            this.SubmitButtonEventDB.Click += new System.EventHandler(this.SubmitButtonEventDB_Click);
            // 
            // dataGridViewEvents
            // 
            this.dataGridViewEvents.AllowUserToAddRows = false;
            this.dataGridViewEvents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewEvents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewEvents.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEvents.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewEvents.Name = "dataGridViewEvents";
            this.dataGridViewEvents.Size = new System.Drawing.Size(800, 194);
            this.dataGridViewEvents.TabIndex = 0;
            // 
            // Create
            // 
            this.Create.Controls.Add(this.materialLabel4);
            this.Create.Controls.Add(this.materialLabel3);
            this.Create.Controls.Add(this.materialLabel2);
            this.Create.Controls.Add(this.materialLabel1);
            this.Create.Controls.Add(this.EventSubmitButton);
            this.Create.Controls.Add(this.dateTimePickerEnd);
            this.Create.Controls.Add(this.dateTimePickerStart);
            this.Create.Controls.Add(this.comboBoxEventStage);
            this.Create.Controls.Add(this.comboBoxEventArtist);
            this.Create.Location = new System.Drawing.Point(4, 22);
            this.Create.Name = "Create";
            this.Create.Padding = new System.Windows.Forms.Padding(3);
            this.Create.Size = new System.Drawing.Size(797, 244);
            this.Create.TabIndex = 1;
            this.Create.Text = "Create";
            this.Create.UseVisualStyleBackColor = true;
            this.Create.Enter += new System.EventHandler(this.Create_Enter);
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(367, 84);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(72, 19);
            this.materialLabel4.TabIndex = 8;
            this.materialLabel4.Text = "End Time";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(367, 33);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(79, 19);
            this.materialLabel3.TabIndex = 7;
            this.materialLabel3.Text = "Start Time";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(48, 83);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(47, 19);
            this.materialLabel2.TabIndex = 6;
            this.materialLabel2.Text = "Stage";
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(48, 33);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(46, 19);
            this.materialLabel1.TabIndex = 5;
            this.materialLabel1.Text = "Artist";
            // 
            // EventSubmitButton
            // 
            this.EventSubmitButton.AutoSize = true;
            this.EventSubmitButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.EventSubmitButton.Depth = 0;
            this.EventSubmitButton.Icon = null;
            this.EventSubmitButton.Location = new System.Drawing.Point(588, 153);
            this.EventSubmitButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.EventSubmitButton.Name = "EventSubmitButton";
            this.EventSubmitButton.Primary = true;
            this.EventSubmitButton.Size = new System.Drawing.Size(71, 36);
            this.EventSubmitButton.TabIndex = 4;
            this.EventSubmitButton.Text = "Submit";
            this.EventSubmitButton.UseVisualStyleBackColor = true;
            this.EventSubmitButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.EventSubmitButton_MouseUp);
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.Location = new System.Drawing.Point(371, 106);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerEnd.TabIndex = 3;
            // 
            // dateTimePickerStart
            // 
            this.dateTimePickerStart.Location = new System.Drawing.Point(371, 56);
            this.dateTimePickerStart.Name = "dateTimePickerStart";
            this.dateTimePickerStart.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerStart.TabIndex = 2;
            // 
            // comboBoxEventStage
            // 
            this.comboBoxEventStage.FormattingEnabled = true;
            this.comboBoxEventStage.Location = new System.Drawing.Point(52, 105);
            this.comboBoxEventStage.Name = "comboBoxEventStage";
            this.comboBoxEventStage.Size = new System.Drawing.Size(136, 21);
            this.comboBoxEventStage.TabIndex = 1;
            // 
            // comboBoxEventArtist
            // 
            this.comboBoxEventArtist.FormattingEnabled = true;
            this.comboBoxEventArtist.Location = new System.Drawing.Point(52, 55);
            this.comboBoxEventArtist.Name = "comboBoxEventArtist";
            this.comboBoxEventArtist.Size = new System.Drawing.Size(136, 21);
            this.comboBoxEventArtist.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.materialTabSelector2);
            this.tabPage2.Controls.Add(this.materialTabControlArtists);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(807, 335);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Artists";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // materialTabSelector2
            // 
            this.materialTabSelector2.BaseTabControl = this.materialTabControlArtists;
            this.materialTabSelector2.Depth = 0;
            this.materialTabSelector2.Location = new System.Drawing.Point(3, -13);
            this.materialTabSelector2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabSelector2.Name = "materialTabSelector2";
            this.materialTabSelector2.Size = new System.Drawing.Size(807, 91);
            this.materialTabSelector2.TabIndex = 3;
            this.materialTabSelector2.Text = "materialTabSelector2";
            // 
            // materialTabControlArtists
            // 
            this.materialTabControlArtists.Controls.Add(this.overviewArtists);
            this.materialTabControlArtists.Controls.Add(this.ArtistCreate);
            this.materialTabControlArtists.Depth = 0;
            this.materialTabControlArtists.Location = new System.Drawing.Point(0, 78);
            this.materialTabControlArtists.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControlArtists.Name = "materialTabControlArtists";
            this.materialTabControlArtists.SelectedIndex = 0;
            this.materialTabControlArtists.Size = new System.Drawing.Size(805, 270);
            this.materialTabControlArtists.TabIndex = 2;
            // 
            // overviewArtists
            // 
            this.overviewArtists.Controls.Add(this.SubmitArtistOverview);
            this.overviewArtists.Controls.Add(this.dataGridViewArtists);
            this.overviewArtists.Location = new System.Drawing.Point(4, 22);
            this.overviewArtists.Name = "overviewArtists";
            this.overviewArtists.Padding = new System.Windows.Forms.Padding(3);
            this.overviewArtists.Size = new System.Drawing.Size(797, 244);
            this.overviewArtists.TabIndex = 0;
            this.overviewArtists.Text = "Overview";
            this.overviewArtists.UseVisualStyleBackColor = true;
            this.overviewArtists.Enter += new System.EventHandler(this.overviewArtists_Enter);
            // 
            // SubmitArtistOverview
            // 
            this.SubmitArtistOverview.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SubmitArtistOverview.AutoSize = true;
            this.SubmitArtistOverview.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SubmitArtistOverview.Depth = 0;
            this.SubmitArtistOverview.Icon = null;
            this.SubmitArtistOverview.Location = new System.Drawing.Point(0, 194);
            this.SubmitArtistOverview.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubmitArtistOverview.Name = "SubmitArtistOverview";
            this.SubmitArtistOverview.Primary = true;
            this.SubmitArtistOverview.Size = new System.Drawing.Size(71, 36);
            this.SubmitArtistOverview.TabIndex = 1;
            this.SubmitArtistOverview.Text = "Submit";
            this.SubmitArtistOverview.UseVisualStyleBackColor = true;
            this.SubmitArtistOverview.Click += new System.EventHandler(this.SubmitArtistOverview_Click);
            // 
            // dataGridViewArtists
            // 
            this.dataGridViewArtists.AllowUserToAddRows = false;
            this.dataGridViewArtists.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewArtists.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewArtists.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewArtists.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewArtists.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewArtists.Name = "dataGridViewArtists";
            this.dataGridViewArtists.Size = new System.Drawing.Size(800, 194);
            this.dataGridViewArtists.TabIndex = 0;
            // 
            // ArtistCreate
            // 
            this.ArtistCreate.BackColor = System.Drawing.Color.White;
            this.ArtistCreate.Controls.Add(this.SubmitButtonArtist);
            this.ArtistCreate.Controls.Add(this.TextBoxArtistDesription);
            this.ArtistCreate.Controls.Add(this.artistNameField);
            this.ArtistCreate.Location = new System.Drawing.Point(4, 22);
            this.ArtistCreate.Name = "ArtistCreate";
            this.ArtistCreate.Padding = new System.Windows.Forms.Padding(3);
            this.ArtistCreate.Size = new System.Drawing.Size(797, 244);
            this.ArtistCreate.TabIndex = 1;
            this.ArtistCreate.Text = "Create";
            // 
            // SubmitButtonArtist
            // 
            this.SubmitButtonArtist.AutoSize = true;
            this.SubmitButtonArtist.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SubmitButtonArtist.Depth = 0;
            this.SubmitButtonArtist.Icon = null;
            this.SubmitButtonArtist.Location = new System.Drawing.Point(515, 173);
            this.SubmitButtonArtist.MouseState = MaterialSkin.MouseState.HOVER;
            this.SubmitButtonArtist.Name = "SubmitButtonArtist";
            this.SubmitButtonArtist.Primary = true;
            this.SubmitButtonArtist.Size = new System.Drawing.Size(71, 36);
            this.SubmitButtonArtist.TabIndex = 3;
            this.SubmitButtonArtist.Text = "Submit";
            this.SubmitButtonArtist.UseVisualStyleBackColor = true;
            this.SubmitButtonArtist.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SubmitButtonArtist_MouseUp);
            // 
            // TextBoxArtistDesription
            // 
            this.TextBoxArtistDesription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.TextBoxArtistDesription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxArtistDesription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxArtistDesription.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.TextBoxArtistDesription.Location = new System.Drawing.Point(29, 84);
            this.TextBoxArtistDesription.Name = "TextBoxArtistDesription";
            this.TextBoxArtistDesription.Size = new System.Drawing.Size(205, 97);
            this.TextBoxArtistDesription.TabIndex = 2;
            this.TextBoxArtistDesription.Text = "Description";
            this.TextBoxArtistDesription.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TextBoxArtistDesription_MouseUp);
            // 
            // artistNameField
            // 
            this.artistNameField.Depth = 0;
            this.artistNameField.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.artistNameField.Hint = "";
            this.artistNameField.Location = new System.Drawing.Point(29, 41);
            this.artistNameField.MaxLength = 32767;
            this.artistNameField.MouseState = MaterialSkin.MouseState.HOVER;
            this.artistNameField.Name = "artistNameField";
            this.artistNameField.PasswordChar = '\0';
            this.artistNameField.SelectedText = "";
            this.artistNameField.SelectionLength = 0;
            this.artistNameField.SelectionStart = 0;
            this.artistNameField.Size = new System.Drawing.Size(205, 23);
            this.artistNameField.TabIndex = 1;
            this.artistNameField.TabStop = false;
            this.artistNameField.Text = "Name";
            this.artistNameField.UseSystemPasswordChar = false;
            this.artistNameField.Click += new System.EventHandler(this.artistNameField_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.materialTabSelector3);
            this.tabPage3.Controls.Add(this.materialTabControl2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(807, 335);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Stages";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // materialTabSelector3
            // 
            this.materialTabSelector3.BaseTabControl = this.materialTabControl2;
            this.materialTabSelector3.Depth = 0;
            this.materialTabSelector3.Location = new System.Drawing.Point(0, -13);
            this.materialTabSelector3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabSelector3.Name = "materialTabSelector3";
            this.materialTabSelector3.Size = new System.Drawing.Size(807, 91);
            this.materialTabSelector3.TabIndex = 5;
            this.materialTabSelector3.Text = "materialTabSelector3";
            // 
            // materialTabControl2
            // 
            this.materialTabControl2.Controls.Add(this.StagesOverview);
            this.materialTabControl2.Controls.Add(this.CreateTabStage);
            this.materialTabControl2.Depth = 0;
            this.materialTabControl2.Location = new System.Drawing.Point(0, 78);
            this.materialTabControl2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl2.Name = "materialTabControl2";
            this.materialTabControl2.SelectedIndex = 0;
            this.materialTabControl2.Size = new System.Drawing.Size(805, 270);
            this.materialTabControl2.TabIndex = 4;
            // 
            // StagesOverview
            // 
            this.StagesOverview.Controls.Add(this.materialRaisedButton1);
            this.StagesOverview.Controls.Add(this.dataGridViewStage);
            this.StagesOverview.Location = new System.Drawing.Point(4, 22);
            this.StagesOverview.Name = "StagesOverview";
            this.StagesOverview.Padding = new System.Windows.Forms.Padding(3);
            this.StagesOverview.Size = new System.Drawing.Size(797, 244);
            this.StagesOverview.TabIndex = 0;
            this.StagesOverview.Text = "Overview";
            this.StagesOverview.UseVisualStyleBackColor = true;
            this.StagesOverview.Enter += new System.EventHandler(this.StagesOverview_Enter);
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.AutoSize = true;
            this.materialRaisedButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Icon = null;
            this.materialRaisedButton1.Location = new System.Drawing.Point(-1, 193);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(71, 36);
            this.materialRaisedButton1.TabIndex = 1;
            this.materialRaisedButton1.Text = "Submit";
            this.materialRaisedButton1.UseVisualStyleBackColor = false;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // dataGridViewStage
            // 
            this.dataGridViewStage.AllowUserToAddRows = false;
            this.dataGridViewStage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewStage.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewStage.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewStage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStage.Location = new System.Drawing.Point(-1, 0);
            this.dataGridViewStage.Name = "dataGridViewStage";
            this.dataGridViewStage.Size = new System.Drawing.Size(800, 194);
            this.dataGridViewStage.TabIndex = 0;
            // 
            // CreateTabStage
            // 
            this.CreateTabStage.BackColor = System.Drawing.Color.White;
            this.CreateTabStage.Controls.Add(this.submitCreationStage);
            this.CreateTabStage.Controls.Add(this.DescriptionStage);
            this.CreateTabStage.Controls.Add(this.materialSingleLineTextFieldStage);
            this.CreateTabStage.Location = new System.Drawing.Point(4, 22);
            this.CreateTabStage.Name = "CreateTabStage";
            this.CreateTabStage.Padding = new System.Windows.Forms.Padding(3);
            this.CreateTabStage.Size = new System.Drawing.Size(797, 244);
            this.CreateTabStage.TabIndex = 1;
            this.CreateTabStage.Text = "Create";
            // 
            // submitCreationStage
            // 
            this.submitCreationStage.AutoSize = true;
            this.submitCreationStage.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.submitCreationStage.Depth = 0;
            this.submitCreationStage.Icon = null;
            this.submitCreationStage.Location = new System.Drawing.Point(530, 154);
            this.submitCreationStage.MouseState = MaterialSkin.MouseState.HOVER;
            this.submitCreationStage.Name = "submitCreationStage";
            this.submitCreationStage.Primary = true;
            this.submitCreationStage.Size = new System.Drawing.Size(71, 36);
            this.submitCreationStage.TabIndex = 3;
            this.submitCreationStage.Text = "Submit";
            this.submitCreationStage.UseVisualStyleBackColor = true;
            this.submitCreationStage.Click += new System.EventHandler(this.submitCreationStage_Click);
            // 
            // DescriptionStage
            // 
            this.DescriptionStage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.DescriptionStage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DescriptionStage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionStage.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.DescriptionStage.Location = new System.Drawing.Point(29, 84);
            this.DescriptionStage.Name = "DescriptionStage";
            this.DescriptionStage.Size = new System.Drawing.Size(205, 97);
            this.DescriptionStage.TabIndex = 2;
            this.DescriptionStage.Text = "Description";
            this.DescriptionStage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DescriptionStage_MouseUp);
            // 
            // materialSingleLineTextFieldStage
            // 
            this.materialSingleLineTextFieldStage.Depth = 0;
            this.materialSingleLineTextFieldStage.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.materialSingleLineTextFieldStage.Hint = "";
            this.materialSingleLineTextFieldStage.Location = new System.Drawing.Point(29, 41);
            this.materialSingleLineTextFieldStage.MaxLength = 32767;
            this.materialSingleLineTextFieldStage.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialSingleLineTextFieldStage.Name = "materialSingleLineTextFieldStage";
            this.materialSingleLineTextFieldStage.PasswordChar = '\0';
            this.materialSingleLineTextFieldStage.SelectedText = "";
            this.materialSingleLineTextFieldStage.SelectionLength = 0;
            this.materialSingleLineTextFieldStage.SelectionStart = 0;
            this.materialSingleLineTextFieldStage.Size = new System.Drawing.Size(205, 23);
            this.materialSingleLineTextFieldStage.TabIndex = 1;
            this.materialSingleLineTextFieldStage.TabStop = false;
            this.materialSingleLineTextFieldStage.Text = "Name";
            this.materialSingleLineTextFieldStage.UseSystemPasswordChar = false;
            this.materialSingleLineTextFieldStage.Click += new System.EventHandler(this.materialSingleLineTextFieldStage_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 473);
            this.Controls.Add(this.mainTabSelector);
            this.Controls.Add(this.mainTabControll);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Event Planner";
            this.mainTabControll.ResumeLayout(false);
            this.EventTab.ResumeLayout(false);
            this.materialTabControlEvents.ResumeLayout(false);
            this.Overview.ResumeLayout(false);
            this.Overview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvents)).EndInit();
            this.Create.ResumeLayout(false);
            this.Create.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.materialTabControlArtists.ResumeLayout(false);
            this.overviewArtists.ResumeLayout(false);
            this.overviewArtists.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArtists)).EndInit();
            this.ArtistCreate.ResumeLayout(false);
            this.ArtistCreate.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.materialTabControl2.ResumeLayout(false);
            this.StagesOverview.ResumeLayout(false);
            this.StagesOverview.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStage)).EndInit();
            this.CreateTabStage.ResumeLayout(false);
            this.CreateTabStage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialTabSelector mainTabSelector;
        private MaterialSkin.Controls.MaterialTabControl mainTabControll;
        private System.Windows.Forms.TabPage EventTab;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private MaterialSkin.Controls.MaterialTabSelector materialTabSelector1;
        private MaterialSkin.Controls.MaterialTabControl materialTabControlEvents;
        private System.Windows.Forms.TabPage Overview;
        private System.Windows.Forms.TabPage Create;
        private System.Windows.Forms.DataGridView dataGridViewEvents;
        private MaterialSkin.Controls.MaterialRaisedButton EventSubmitButton;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.DateTimePicker dateTimePickerStart;
        private System.Windows.Forms.ComboBox comboBoxEventStage;
        private System.Windows.Forms.ComboBox comboBoxEventArtist;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialRaisedButton SubmitButtonEventDB;
        private MaterialSkin.Controls.MaterialTabSelector materialTabSelector2;
        private MaterialSkin.Controls.MaterialTabControl materialTabControlArtists;
        private System.Windows.Forms.TabPage overviewArtists;
        private MaterialSkin.Controls.MaterialRaisedButton SubmitArtistOverview;
        private System.Windows.Forms.DataGridView dataGridViewArtists;
        private System.Windows.Forms.TabPage ArtistCreate;
        private MaterialSkin.Controls.MaterialSingleLineTextField artistNameField;
        private System.Windows.Forms.RichTextBox TextBoxArtistDesription;
        private MaterialSkin.Controls.MaterialRaisedButton SubmitButtonArtist;
        private MaterialSkin.Controls.MaterialTabSelector materialTabSelector3;
        private MaterialSkin.Controls.MaterialTabControl materialTabControl2;
        private System.Windows.Forms.TabPage StagesOverview;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private System.Windows.Forms.DataGridView dataGridViewStage;
        private System.Windows.Forms.TabPage CreateTabStage;
        private MaterialSkin.Controls.MaterialRaisedButton submitCreationStage;
        private System.Windows.Forms.RichTextBox DescriptionStage;
        private MaterialSkin.Controls.MaterialSingleLineTextField materialSingleLineTextFieldStage;
    }
}

