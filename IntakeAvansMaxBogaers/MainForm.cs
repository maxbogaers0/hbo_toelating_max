﻿using IntakeAvansMaxBogaers.Classes;
using MaterialSkin;
using MaterialSkin.Controls;

using System;
using System.Data;
using System.Windows.Forms;

namespace IntakeAvansMaxBogaers
{
    public partial class mainForm : MaterialForm
    {
        private Data data = new Data();
        public mainForm()
        {
            InitializeComponent();
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.DARK;
            //materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            this.StartDataGridView();
        }

        private void Create_Enter(object sender, EventArgs e)
        {
            comboBoxEventArtist.DataSource = data.getAll(data.artists);
            comboBoxEventArtist.DisplayMember = "name";
            comboBoxEventArtist.ValueMember = "artist_id";
            comboBoxEventStage.DataSource = data.getAll(data.stages);
            comboBoxEventStage.DisplayMember = "name";
            comboBoxEventStage.ValueMember = "stage_id";
            dateTimePickerStart.Format = DateTimePickerFormat.Custom;
            dateTimePickerStart.CustomFormat = "dd/MM/yyyy hh:mm";
            dateTimePickerEnd.Format = DateTimePickerFormat.Custom;
            dateTimePickerEnd.CustomFormat = "dd/MM/yyyy hh:mm";

        }

        private void EventSubmitButton_MouseUp(object sender, MouseEventArgs e)
        {
            int artist = (int) comboBoxEventArtist.SelectedValue;
            int stage  =  (int) comboBoxEventStage.SelectedValue;
            DateTime start = dateTimePickerStart.Value.Date;
            DateTime end = dateTimePickerEnd.Value.Date;
            new Event(artist,stage,start,end);
        }

        private void Overview_Enter(object sender, EventArgs e)
        {
            this.StartDataGridView();
        }

        private void Overview_Leave(object sender, EventArgs e)
        {
            DataTable newData = (DataTable)dataGridViewEvents.DataSource;
            data.UpdateTable(newData);
        }

        private void StartDataGridView()
        {
            dataGridViewEvents.Columns.Clear();
            dataGridViewEvents.AutoGenerateColumns = false;
            DataTable table = data.getAll(data.events);
            DataGridViewComboBoxColumn artist = createCollumn(data.getAll(data.artists), "artist");
            DataGridViewComboBoxColumn stage = createCollumn(data.getAll(data.stages), "stage");
            DataGridViewTextBoxColumn start = new DataGridViewTextBoxColumn();
            start.HeaderText = "Start Time";
            start.DataPropertyName = "start_time";
            DataGridViewTextBoxColumn end = new DataGridViewTextBoxColumn();
            end.HeaderText = "Start Time";
            end.DataPropertyName = "start_time";
            dataGridViewEvents.DataSource = table;
            dataGridViewEvents.Columns.AddRange(artist, stage, start, end);

        }

        private void SubmitButtonEventDB_Click(object sender, EventArgs e)
        {
            DataTable newData = (DataTable)dataGridViewEvents.DataSource;
            data.UpdateTable(newData);
        }

        private void overviewArtists_Enter(object sender, EventArgs e)
        {
            dataGridViewArtists.DataSource = data.getAll(data.artists);
            dataGridViewArtists.Columns["artist_id"].Visible = false;
        }

        private void SubmitButtonArtist_MouseUp(object sender, MouseEventArgs e)
        {
            String name = artistNameField.Text;
            String desription = TextBoxArtistDesription.Text;
            new Artist(name,desription);
        }

        private void SubmitArtistOverview_Click(object sender, EventArgs e)
        {
            DataTable newData = (DataTable)dataGridViewArtists.DataSource;
            data.UpdateTable(newData);
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            DataTable newData = (DataTable)dataGridViewStage.DataSource;
            data.UpdateTable(newData);
        }

        private void StagesOverview_Enter(object sender, EventArgs e)
        {
            dataGridViewStage.DataSource = data.getAll(data.stages);
                dataGridViewStage.Columns["stage_id"].Visible = false;
            }

        private void submitCreationStage_Click(object sender, EventArgs e)
        {
            
            String name = materialSingleLineTextFieldStage.Text;
            String desription = DescriptionStage.Text;
            new Stage(name, desription);
        }
        private DataGridViewComboBoxColumn createCollumn(DataTable source, String name)
        {
            DataGridViewComboBoxColumn col = new DataGridViewComboBoxColumn();
            col.DataSource = source;
            col.HeaderText = char.ToUpper(name[0]) + name.Substring(1); ;
            col.DataPropertyName = name+"_id";
            col.ValueMember = name+"_id";
            col.DisplayMember = "name";

            return col;
        }

        private void artistNameField_Click(object sender, EventArgs e)
        {
            artistNameField.Clear();
        }


        private void materialSingleLineTextFieldStage_Click(object sender, EventArgs e)
        {
            materialSingleLineTextFieldStage.Clear();
        }

        private void TextBoxArtistDesription_MouseUp(object sender, MouseEventArgs e)
        {
            TextBoxArtistDesription.Clear();
        }

        private void DescriptionStage_MouseUp(object sender, MouseEventArgs e)
        {
            DescriptionStage.Clear();
        }
    }

}
